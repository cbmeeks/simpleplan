module LayoutHelper
  
  def fluid()
    content_for(:fluid) { "-fluid" }
  end

  def title(title)
    content_for(:title) {title}
  end

  def funny_name
    names = ["John Rambo", 
             "Your full name",
             "Type your name here",
             "FirstName LastName",
             "Samus Aran",
             "Moe Howard",
             "Larry Fine",
             "Curly Howard",
             "Shemp Howard",
             "John Doe"]
    names[rand(names.size)]
  end


end