module SessionsHelper
  
  def random_email_placeholder_text
    emails = ["somebody@example.com", 
              "me@example.com",
              "Your email here...",
              "seriousbusinessdude@example.com",
              "immapc@example.com",
              "immamac@example.com"]
    emails[rand(emails.size)]
  end
  
end
