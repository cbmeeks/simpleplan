class UsersController < ApplicationController
  def new
    if current_user
      flash[:info] = "Already logged in"
      render "add_new_user"
    else
      @user = User.new
    end
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:success] = random_register_quote
      session[:user_id] = @user.id
      redirect_to root_url
    else
      render "new"
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update_attributes(params[:user])
      flash[:success] = "Your profile was updated successfully."
      redirect_to profile_path
    else
      render "edit"
    end
  end


  def add_new_user
  end

  private
  
  def random_register_quote
    quotes = ["Welcome aboard!", 
              "<strong>WOW!</strong> Thanks for signing up!".html_safe,
              "We knew you couldn't resist!",
              "Thank you for signing up!",
              "<strong>Fantastic!</strong> You are now a member of an elite team of project planners.".html_safe]
    quotes[rand(quotes.size)]
  end


end
