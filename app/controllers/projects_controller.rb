class ProjectsController < ApplicationController
  respond_to :html, :json, :xml
  
  def index
    @projects = Project.all
    respond_with @projects
  end
  
  
end
