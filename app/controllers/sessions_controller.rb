class SessionsController < ApplicationController
  def new
    if current_user
      flash[:info] = "Already logged in"
      redirect_to root_url
    end
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      flash[:success] = random_login_quote
      redirect_to root_url
    else
      flash.now[:error] = random_invalid_login_quote
      render "new"
    end
  end
  
  def destroy
    session[:user_id] = nil
    flash[:warning] = random_logout_quote
    redirect_to root_url
  end
  
  
  private
  
  def random_login_quote
    quotes = ["Logged in successfully!", 
              "Awesome! Thanks for logging in.",
              "Welcome back, we missed you.",
              "Where have you been?",
              ":-)"]
    quotes[rand(quotes.size)]
  end
  
  def random_logout_quote
    quotes = ["Logged out successfully!", 
              "Sorry to see you go.  :-(",
              "Come back real soon!",
              "Leaving already?",
              "Don't worry.  We'll keep things running while you're gone.",
              "We understand.  You have more <strong>important</strong> things to do.".html_safe]
    quotes[rand(quotes.size)]
  end

  def random_invalid_login_quote
    quotes = ["<strong>Sorry</strong>, but that login was invalid.".html_safe, 
              "Hmmm.  Something is wrong with that login.",
              "Would you mind trying that again?",
              "It's OK. I'm sure you just typed that incorrectly.",
              "I'm sorry, Dave. I'm afraid I can't do that.",
              "Hey, don't worry.  But would you mind trying that again?"]
    quotes[rand(quotes.size)]
  end

  
end
