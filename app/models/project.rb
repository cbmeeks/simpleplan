class Project < ActiveRecord::Base
  
  # attributes
  attr_accessible :name, :description, :active
  
  # associations
  
  # validations
  validates_presence_of :name
  
  
end
