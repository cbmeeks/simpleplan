class Company < ActiveRecord::Base
  # attributes
  attr_accessible :name

  # associations
  has_many :users
  
  # validations
  validates_presence_of :name, :message => "required"
  
end
