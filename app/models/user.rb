class User < ActiveRecord::Base
  # attributes
  attr_accessible :email, :password, :password_confirmation, :name, :timezone
  attr_accessor :password

  # associations
  belongs_to :company
  
  # callbacks
  before_save :encrypt_password
  
  # validations
  validates_presence_of :email, :message => "Sorry, a valid email is required."
  validates_uniqueness_of :email, :message => "Looks like that email is taken."
  
  validates_presence_of :password, :on => :create, :message => "Please enter a password."
  validates_confirmation_of :password, :message => "Oh no!  Confirmation doesn't match."
  validates_length_of :password, :within => 6..24
  
  # private methods
  private
  
  def self.authenticate(email, password)

    user = User.find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.salt)
      user
    else
      nil
    end    
  end
  
  def encrypt_password
    if password.present?
      self.salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, salt)
    end
  end
  
  
end
