# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
	$('#projects-table').tablesorter({ sortList: [[0,0]] })
	
	$('.animate-width-to').each ->
	    length = $(@).data("animate-length")
	    width = $(@).data("animate-width-to")
	    $(@).animate(width: width, length, '')