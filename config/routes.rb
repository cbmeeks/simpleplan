Simpleplan::Application.routes.draw do
  get "login" => "sessions#new", :as => "login"
  get "logout" => "sessions#destroy", :as => "logout"
  get "signup" => "users#new", :as => "signup"

  get "profile" => "users#edit", :as => "profile"

  resources :projects
  resources :sessions
  resources :users

  root :to => 'welcome#index'
end
